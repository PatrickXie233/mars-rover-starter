O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

1. codeReview: In today's codeReview, I think my code has many areas that can be improved: method reuse, code simplicity, code formatting, encapsulation, void detection, Observer pattern
2. Unit testing: mainly using ready-made code, and writing test cases on this basis to test its functionality
3. TDD: Test-driven development, first define what functions I want to achieve, then write test cases according to the functions, and then add new functions under the premise of testing. The main process is from Red (compilation failure) to Green (compilation success) and then to Refactor (refactoring)
4. TDD Practice: We have practiced through a few simple examples how to go from a business, to writing Tasks, to writing test cases, and finally implementing them, bringing us a different development experience

R (Reflective): Please use one word to express your feelings about today's class.
面面俱到
I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?
Some APIs tested may not be particularly familiar
D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
I may still rarely achieve frequent Git submissions now. In future exercises, I need to make improvements and write logs to make the code traceable.
