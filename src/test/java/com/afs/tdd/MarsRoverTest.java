package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class MarsRoverTest {
    @Test
    void should_change_location_y_plus1_when_excuteCommend_given_location_00N_command_M() {

        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.North));

        marsRover.excuteCommend(Commend.Move);
        Location nowLocation = marsRover.getLocation();

        Assertions.assertEquals(new Location(0, 1, Direction.North), nowLocation);
    }

    @Test
    void should_change_location_y_reduce1_when_excuteCommend_given_location_00S_command_M() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.South));

        marsRover.excuteCommend(Commend.Move);
        Location nowLocation = marsRover.getLocation();

        Assertions.assertEquals(new Location(0, -1, Direction.South), nowLocation);
    }

    @Test
    void should_change_location_x_plus1_when_excuteCommend_given_location_00E_command_M() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.East));

        marsRover.excuteCommend(Commend.Move);
        Location nowLocation = marsRover.getLocation();

        Assertions.assertEquals(new Location(1, 0, Direction.East), nowLocation);
    }

    @Test
    void should_change_location_x_reduce1_when_excuteCommend_given_location_00W_command_M() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.West));

        marsRover.excuteCommend(Commend.Move);
        Location nowLocation = marsRover.getLocation();

        Assertions.assertEquals(new Location(-1, 0, Direction.West), nowLocation);
    }

    @Test
    void should_change_location_direction_to_W_when_excuteCommend_given_location_00N_command_L() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.North));
        marsRover.excuteCommend(Commend.TurnLeft);
        Location nowLocation = marsRover.getLocation();
        Assertions.assertEquals(new Location(0, 0, Direction.West), nowLocation);
    }

    @Test
    void should_change_location_direction_to_N_when_excuteCommend_given_location_00E_command_L() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.East));
        marsRover.excuteCommend(Commend.TurnLeft);
        Location nowLocation = marsRover.getLocation();
        Assertions.assertEquals(new Location(0, 0, Direction.North), nowLocation);
    }

    @Test
    void should_change_location_direction_to_E_when_excuteCommend_given_location_00S_command_L() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.South));
        marsRover.excuteCommend(Commend.TurnLeft);
        Location nowLocation = marsRover.getLocation();
        Assertions.assertEquals(new Location(0, 0, Direction.East), nowLocation);
    }

    @Test
    void should_change_location_direction_to_S_when_excuteCommend_given_location_00W_command_L() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.West));
        marsRover.excuteCommend(Commend.TurnLeft);
        Location nowLocation = marsRover.getLocation();
        Assertions.assertEquals(new Location(0, 0, Direction.South), nowLocation);
    }

    @Test
    void should_change_location_direction_to_E_when_excuteCommend_given_location_00N_command_R() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.North));
        marsRover.excuteCommend(Commend.TurnRight);
        Location nowLocation = marsRover.getLocation();
        Assertions.assertEquals(new Location(0, 0, Direction.East), nowLocation);
    }

    @Test
    void should_change_location_direction_to_S_when_excuteCommend_given_location_00E_command_R() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.East));
        marsRover.excuteCommend(Commend.TurnRight);
        Location nowLocation = marsRover.getLocation();
        Assertions.assertEquals(new Location(0, 0, Direction.South), nowLocation);
    }

    @Test
    void should_change_location_direction_to_W_when_excuteCommend_given_location_00S_command_R() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.South));
        marsRover.excuteCommend(Commend.TurnRight);
        Location nowLocation = marsRover.getLocation();
        Assertions.assertEquals(new Location(0, 0, Direction.West), nowLocation);
    }

    @Test
    void should_change_location_direction_to_N_when_excuteCommend_given_location_00W_command_R() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.South));
        marsRover.excuteCommend(Commend.TurnRight);
        Location nowLocation = marsRover.getLocation();
        Assertions.assertEquals(new Location(0, 0, Direction.West), nowLocation);
    }
    @Test
    void should_change_location_direction_to_12N_when_excuteCommend_given_location_00N_command_MRMLM() {
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.North));
        ArrayList<Commend> commendArrayList = new ArrayList<>();
        commendArrayList.add(Commend.Move);
        commendArrayList.add(Commend.TurnRight);
        commendArrayList.add(Commend.Move);
        commendArrayList.add(Commend.TurnLeft);
        commendArrayList.add(Commend.Move);

        marsRover.excuteCommendList(commendArrayList);
        Location nowLocation = marsRover.getLocation();
        Assertions.assertEquals(new Location(1, 2, Direction.North), nowLocation);
    }
}
