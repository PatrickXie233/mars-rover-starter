package com.afs.tdd;

import java.util.ArrayList;

public class MarsRover {
    private Location location = new Location(0, 0, Direction.North);

    public MarsRover(Location location) {
        this.location = location;
    }

    public void excuteCommend(Commend commend) {
        Location location = this.getLocation();
        if (commend.equals(Commend.Move)) {
            this.location = Move(location);
        } else if (commend.equals(Commend.TurnLeft)) {
            this.location = turnLeft(location);
        } else if (commend.equals(Commend.TurnRight)) {
            this.location = trunRight(location);
        }
    }

    private Location trunRight(Location location) {
        switch (location.getDirection()) {
            case South:
                location.setDirection(Direction.West);
                break;
            case West:
                location.setDirection(Direction.North);
                break;
            case East:
                location.setDirection(Direction.South);
                break;
            case North:
                location.setDirection(Direction.East);
                break;
        }
        return location;
    }

    public Location turnLeft(Location location) {
        switch (location.getDirection()) {
            case South:
                location.setDirection(Direction.East);
                break;
            case West:
                location.setDirection(Direction.South);
                break;
            case East:
                location.setDirection(Direction.North);
                break;
            case North:
                location.setDirection(Direction.West);
                break;
        }
        return location;
    }

    public Location Move(Location location) {
        switch (location.getDirection()) {
            case West:
                location.setCoordinateX(location.getCoordinateX() - 1);
                break;
            case East:
                location.setCoordinateX(location.getCoordinateX() + 1);
                break;
            case South:
                location.setCoordinateY(location.getCoordinateY() - 1);
                break;
            case North:
                location.setCoordinateY(location.getCoordinateY() + 1);
                break;
        }
        return location;
    }

    public Location getLocation() {
        return location;
    }

    public void excuteCommendList(ArrayList<Commend> commendArrayList) {
        commendArrayList.forEach(this::excuteCommend);
    }
}
